#!ruby
require "active_support/time"

MIN_YEAR = 1999
MAX_YEAR = Date.today.year + 2.to_i
MAKES = %w(Ford blah Chevrolet)
MODELS = %w(foo Impala Focus)

def normalize_year(year)
  year = year.to_i
  if year < MIN_YEAR || year > MAX_YEAR
    return year.to_s
  end
  year
end

def normalize_trim(model, trim)
  if trim === "blank"
    trim = nil
  elsif trim.empty?
    split_string = model.split(" ")
    trim = split_string.last.upcase
  elsif trim.length == 2
    trim.upcase!
  end
  trim
end

def normalize_titlecase(array, value)
  downcased = array.map(&:downcase)
  split = value.split(" ")
  index = downcased.index{|correct_make| correct_make.include?(split[0].downcase)}
  if index == nil
    value
  else
    array[index].to_s
  end
end

def normalize_data(input)

  input.map do |key, value|
    year = normalize_year(input[:year])
    make = normalize_titlecase(MAKES, input[:make])
    model = normalize_titlecase(MODELS, input[:model])
    trim  = normalize_trim(input[:model], input[:trim])
    return {
      :year => year,
      :make => make,
      :model => model,
      :trim => trim
    }
  end
end

examples = [
  [{ :year => '2018', :make => 'fo', :model => 'focus', :trim => 'blank' },
   { :year => 2018, :make => 'Ford', :model => 'Focus', :trim => nil }],
  [{ :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' },
   { :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' }],
  [{ :year => '1999', :make => 'Chev', :model => 'IMPALA', :trim => 'st' },
   { :year => 1999, :make => 'Chevrolet', :model => 'Impala', :trim => 'ST' }],
  [{ :year => '2000', :make => 'ford', :model => 'focus se', :trim => '' },
   { :year => 2000, :make => 'Ford', :model => 'Focus', :trim => 'SE' }]
]

examples.each_with_index do |(input, expected_output), index|
  if (output = normalize_data(input)) == expected_output
    puts "Example #{index + 1} passed!"
  else
    puts "Example #{index + 1} failed,
          Expected: #{expected_output.inspect}
          Got:      #{output.inspect}"
  end
end